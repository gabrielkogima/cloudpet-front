import Logo from '../../assets/logo.svg';
import Landing from '../../assets/landing.svg';

import '../../styles/home.css';

function App() {
  return (
    <div className="app">
      <div className="container">
        <div className="logo">
          <img src={Logo} alt="CloudPet logo" />
          <h1>CloudPet</h1>
        </div>

        <div className="landing">
          <img src={Landing} alt="CloudPet landing image" />
        </div>

        <div className="useless-info">
          <h1>Controle . Organize . Agende</h1>
          <h1>Tudo para seu petshop</h1>
        </div>
      </div>
    </div>
  );
}

export default App;
